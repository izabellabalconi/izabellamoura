public class Elfo extends Personagem {
    private int indiceFlecha;
    protected int experiencia, qtdExperienciaPorAtaque;
    private static int qtdElfos; // contador de instancias exercicio lista 6

    {
        this.inventario = new Inventario(2);
        this.indiceFlecha = 1;
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;

        this.vida = 100;
    }

    public Elfo(String nome) {
        super(nome);
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
        Elfo.qtdElfos++;
    }
// exercicio lista 6:
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
//exercicio lista 6:
    public static int getQtdElfos() {
        return Elfo.qtdElfos;
    }

    public int getExperiencia() {
        return this.experiencia;
    }

    protected void aumentarXp() {
        //experiencia++;
        experiencia = experiencia + this.qtdExperienciaPorAtaque;
    }

    public Item getFlecha() {
        return this.inventario.obter(this.indiceFlecha);
    }

    public int getQtdFlechas() {
        return this.getFlecha().getQuantidade();
    }

    // DRY - Don't Repeat Yourself
    private boolean podeAtirarFlecha() {
        return this.getFlecha().getQuantidade() > 0;
    }

    public void atirarFlecha(Dwarf dwarf) {
        int qtdAtual = this.getFlecha().getQuantidade();
        if (podeAtirarFlecha()) {
            this.getFlecha().setQuantidade(qtdAtual - 1);
            //experiencia = experiencia + 1;
            this.aumentarXp();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }

    public String imprimirResumo(){
        return "Elfo";
    }

}
