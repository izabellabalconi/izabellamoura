import java.util.ArrayList;
import java.util.Arrays;
public class ElfoDaLuz extends Elfo{
    private int contadorAtaques;
    private int qtdAtaques = 0;
    private final double QTD_VIDA_GANHA = 10;

    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList(
                "Espada de Galvorn"
            ));

    public ElfoDaLuz(String nome) {
        super(nome);
        this.qtdDano = 21;
        super.ganharItem(new ItemPermanente(1, DESCRICOES_OBRIGATORIAS.get(0)));
    }

    private boolean devePerderVida() {
        return qtdAtaques % 2 == 1;
    }

    private void ganharVida() {
        vida += QTD_VIDA_GANHA;
    }

    public void perderItem(Item item) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if (possoPerder) {
            super.perderItem(item);
        }
    }

    private Item getEspada() {
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }

    public void atacarComEspada(Dwarf dwarf) {
        Item espada = getEspada();
        // esse if nao e' mais necessario desde que corrigimos com o ItemSempreExistente, mas deixei aqui pra ficar claro (para aqueles que nao implementaram o cenario de testes que alterava a quantidade)
        if (espada.getQuantidade() > 0) {
            qtdAtaques++;
            dwarf.sofrerDano();
            if (devePerderVida()) {
                sofrerDano();
            } else {
                ganharVida();
            }
        }
    }

    /*
     * 
     public ElfoDaLuz(String nome) {
        super(nome); 
        this.qtdExperienciaPorAtaque = 1;
        this.inventario.adicionar(new Item(1, "Espada de Galvorn"));
        this.contadorAtaques = 0;
    }

    public void atacarComEspada(){
        contadorAtaques++;
        if (contadorAtaques %2 == 0){
            this.vida = getVida() + 10;}
        else
        {this.vida = getVida() - 21;}
        this.aumentarXp();
        this.sofrerDano();
    }
    */
   

}

