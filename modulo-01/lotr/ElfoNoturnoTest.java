import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest{
     /*
      * 
      @Test
    public void devePerderVidaAoAtirarFlecha() {
        // Arrange
        ElfosNoturno umElfo = new ElfosNoturno("Legolas");
        Dwarf dwarf = new Dwarf("Barindrod Shatterdigger");
        // Act
        umElfo.atirarFlecha(dwarf);
        
        // Assert
        assertEquals (umElfo.getVida() , 85);
    }
    */
    @Test
    public void deveGanhar3XpAoAtirarFlecha(){
        ElfoNoturno elfonoturno = new ElfoNoturno("elfinho");
        Dwarf anao = new Dwarf("anaozinho");
        
        elfonoturno.atirarFlecha(anao);
        
        assertEquals(3, elfonoturno.getExperiencia());
    }
    
}
