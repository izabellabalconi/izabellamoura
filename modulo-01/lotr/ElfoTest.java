import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Barindrod Shatterdigger");
        // Act
        umElfo.atirarFlecha(dwarf);
        // Assert
        assertEquals(1, umElfo.getExperiencia());
        // Lei de Demeter - Law of Demeter
        //assertEquals(41, umElfo.getFlecha().getQuantidade());
        assertEquals(1, umElfo.getQtdFlechas());
    }

    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Barindrod Shatterdigger");
        // Act
        umElfo.atirarFlecha(dwarf);
        umElfo.atirarFlecha(new Dwarf("Kronabela Merryarm"));
        umElfo.atirarFlecha(new Dwarf("Weramoren Lightbreaker"));
        // Assert
        assertEquals(2, umElfo.getExperiencia());
        // Lei de Demeter - Law of Demeter
        //assertEquals(41, umElfo.getFlecha().getQuantidade());
        assertEquals(0, umElfo.getQtdFlechas());
    }

    @Test
    public void elfosNascemCom2Flechas() {
        Elfo umElfo = new Elfo("Elrond");
        assertEquals(2, umElfo.getQtdFlechas());
    }
    
    @Test
    public void atirarFlechaEmDwarfTiraVida() {
        // Arrange
        Elfo umElfo = new Elfo("Alanis Erna");
        Dwarf dwarf = new Dwarf("Skatmok Greatjaw");
        // Act
        umElfo.atirarFlecha(dwarf);
        // Assert
        assertEquals(100.0, dwarf.getVida(), .00001);
        assertEquals(1, umElfo.getExperiencia());
        assertEquals(1, umElfo.getQtdFlechas());
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado() {
        Elfo elfo = new Elfo("Dain Yesxalim");
        assertEquals(Status.RECEM_CRIADO, elfo.getStatus());
    }
}






















