import java.util.ArrayList;
import java.util.Arrays;
public class ElfoVerde extends Elfo {

    private ArrayList<String> descricoesValidas = new ArrayList<>(
            Arrays.asList(
                "Espada de aço valiriano",
                "Arco de Vidro",
                "Flecha de Vidro")
        );

    public ElfoVerde(String nome) {
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }

    protected void aumentarXp() {
        experiencia = experiencia + 2;
    }

    public void ganharItem(Item item){
        boolean descricaoValida = descricoesValidas.contains(item.getDescricao());

        if (descricaoValida) {
            this.inventario.adicionar(item);
        }
    }
    public void perderItem(Item item){
        boolean descricaoValida = descricoesValidas.contains(item.getDescricao());

        if (descricaoValida) {
            this.inventario.remover(item);
        }
    }

}
