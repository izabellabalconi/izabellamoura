import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    @Test
    public void deveGanhar2XpAoAtirarFlecha(){
        ElfoVerde celeborn = new ElfoVerde("Celeborn");
        celeborn.atirarFlecha(new Dwarf("Balin"));
        assertEquals(2, celeborn.getExperiencia());
    }
    
    
}
