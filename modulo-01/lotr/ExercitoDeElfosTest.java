import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoDeElfosTest{
    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfoVerde("Green Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }

    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }

    @Test
    public void naoPodeAlistarElfo() {
        Elfo elfo = new Elfo("Common Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void naoPodeAlistarElfoDaLuz() {
        Elfo elfo = new ElfoDaLuz("Light Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void buscarElfosRecemCriadosExistindo() {
        Elfo recemCriado = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(recemCriado);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(recemCriado)
            );
        assertEquals(esperado, exercito.buscar(Status.RECEM_CRIADO));
    }

    @Test
    public void buscarElfosVivosNaoExistindo() {
        Elfo recemCriadoTrocaPraMorto = new ElfoNoturno("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        // simular morte
        recemCriadoTrocaPraMorto.getFlecha().setQuantidade(7);
        for (int i = 0; i < 7; i++) {
            recemCriadoTrocaPraMorto.atirarFlecha(new Dwarf("Gimli"));
        }
        exercito.alistar(recemCriadoTrocaPraMorto);
        assertNull(exercito.buscar(Status.RECEM_CRIADO));
    }

    @Test
    public void buscarElfosMortosExistindo() {
        Elfo recemCriadoTrocaPraMorto = new ElfoNoturno("Undead Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        // simular morte
        recemCriadoTrocaPraMorto.getFlecha().setQuantidade(7);
        for (int i = 0; i < 7; i++) {
            recemCriadoTrocaPraMorto.atirarFlecha(new Dwarf("Gimli"));
        }
        exercito.alistar(recemCriadoTrocaPraMorto);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(recemCriadoTrocaPraMorto)
            );
        assertEquals(esperado, exercito.buscar(Status.MORTO));
    }    

    @Test
    public void buscarElfosMortosNaoExistindo() {
        Elfo recemCriado = new ElfoVerde("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(recemCriado);
        assertNull(exercito.buscar(Status.MORTO));
    }
    
    
}
