import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemPermanenteTest{
 
    @Test
    public void itemSempreExistenteNaoPodeSerZerado() {
        Item item = new ItemPermanente(1, "Anduril");
        item.setQuantidade(0);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemSempreExistenteNaoPodeSerNegativo() {
        Item item = new ItemPermanente(1, "Anduril");
        item.setQuantidade(-10);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemSempreExistentePodeReceberOutraQuantidade() {
        Item item = new ItemPermanente(2, "Escudo de madeira");
        item.setQuantidade(1);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemSempreExistenteNaoPodeSerCriadoComZero() {
        Item item = new ItemPermanente(0, "Escudo de madeira");
        assertEquals(1, item.getQuantidade());
    }

}
