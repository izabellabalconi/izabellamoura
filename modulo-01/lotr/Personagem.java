public abstract class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida, qtdDano;

    {
        status = Status.RECEM_CRIADO;
        this.inventario = new Inventario(0);
        this.qtdDano = 0.0;
    }

    protected Personagem(String nome) {
        this.nome = nome;
    }

    protected void setNome(String nome) {
        this.nome = nome;
    }

    protected String getNome() {
        return this.nome;
    }

    protected Status getStatus() {
        return this.status;
    }

    protected Inventario getInventario() {
        return this.inventario;
    }

    protected double getVida() {
        return this.vida;
    }

    protected void ganharItem(Item item){
        this.inventario.adicionar(item);
    }

    protected void perderItem(Item item){
        this.inventario.remover(item);
    }

    protected double calcularDano() {
        return this.qtdDano;
    }

    protected void sofrerDano() {
        this.status = Status.SOFREU_DANO;
        // enquanto tiver vida pra perder, sofre o dano calculado, senão apenas decrementa a quantidade de vida restante para zerá-la.
        this.vida -= this.vida >= this.qtdDano ? calcularDano() : this.vida;
        if (this.vida == 0.0) {
            this.status = Status.MORTO;
        }
    }
    
    public abstract String imprimirResumo();
    
}