class Pokemon{

  constructor( objVindoDaApi ) {
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this._altura = objVindoDaApi.height
  }

  // pokemon.altura
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes#Prototype_methods
  get altura() {
    // transformar altura para cm (originalmente vem em dezenas de cm)
    return this._altura * 10
  }

}
