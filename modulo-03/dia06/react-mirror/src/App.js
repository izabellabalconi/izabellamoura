import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ListaAvaliacoes from './components/listaAvaliacoes';
import telaDetalheEpisodio from './components/telaDetalheEpisodio'
import PaginaInicial from './components/paginaInicial';

import './App.css';
import TodosEpisodios from './components/todosEpisodios';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <section className="screen">
              <Route path="/" exact component={ PaginaInicial }/>
              <Route path="/avaliacoes" component={ ListaAvaliacoes }/>
              <Route path="/todosEps" component={ TodosEpisodios }/>
              <Route path="/episodio/id" component= {telaDetalheEpisodio} />

            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App; 


