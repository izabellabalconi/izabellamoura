import React, {Component} from 'react'

const EpisodioUi = props => {
    const { episodio } = props
    return (
      <React.Fragment>
           <h2>{ episodio.nome }</h2>
           <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
           <span>Já assisti? { episodio.assistido ? 'sim' : 'não' }, { episodio.qtdVezesAssistido } ve(zes)</span>
           <span>{ episodio.duracaoEmMin }</span>
           <span>{ episodio.temporadaEpisodio }</span>
           <span>{ episodio.nota || 'sem nota' }</span>
         </React.Fragment>
    )
  }
  
  export default EpisodioUi
  
  