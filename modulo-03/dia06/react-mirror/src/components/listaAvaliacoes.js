import React, { Component } from 'react';
import {Link} from 'react-router-dom';

function ordenacaoTemp(a, b) {
  return a.temporada - b.temporada
}

function ordenacaoEpi(a, b) {
return a.ordemEpisodio - b.ordemEpisodio
}

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state
  const listaOrdenada = listaEpisodios.avaliados.sort( ordenacaoTemp,ordenacaoEpi )
      
      return listaOrdenada.map( e => <Link to={{pathname : `/episodio/id`,state :{ e }}}> <li key={ e.id }> { `${ e.nome} - ${ e.nota }` }</li> </Link>)

}


export default ListaAvaliacoes
/*
const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state
return listaEpisodios.avaliados.map( e =>
    <li key={ e.id }>
       <Link to={ { pathname: `/episodio/${ e.id }`, state: { e }}}>
        { `${ e.nome } - ${ e.nota }` }
      </Link>
    </li>
   )
   }
   export default ListaAvaliacoes


/*
const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state
  return listaEpisodios.avaliados.map( e => <li key={ e.id }>{ `${ e.nome } - ${ e.nota }` }</li> )
  
}

export default ListaAvaliacoes

/* 
   p( e => <Link className="link" to={{ pathname: "/episodio", state: listaEpisodios }} value={ e.id }><li key={ e.id }>{ `${ e.nome } -  ${ e.temporada } - ${ e.ordemEpisodio }` }</li></Link> ) }
 /* export default class ListaAvaliacoes extends Component{
  constructor( props ){
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      lista: [],
      notas: []
    }
  }

  componentDidMount() {
    this.episodiosApi.buscarNotas()
      .then( n => {
        this.setState({
          notas: n.data
        })        
      })

    this.episodiosApi.buscar()
      .then( episodiosDoServidor => {
        this.listaEpisodios = new ListaEpisodios( episodiosDoServidor.data )

        let episodiosNotas = this.listaEpisodios.retornarTodos.map(e=> {
          return {
            episodio: e,
            nota: this.state.notas.filter( n => n.episodioId === e.id ).map( n => n.nota )[0]
          }
        })
        
        this.setState({
          lista: episodiosNotas
        })       
      })
    }
  } */