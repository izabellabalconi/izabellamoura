import Detalhes from "./detalhes";

export default class listaDetalhes {
  constructor( DetalhesDoServidor ) {
    this.todos = DetalhesDoServidor.map( e => new Detalhes( e.id, e.notaImdb, e.sinopse, e.dataEstreia))
  }

}