import React, { Component } from 'react'

import EpisodiosApi from '../episodiosApi'

import ListaEpisodios from '../lista-episodios'
import EpisodioUi from '../components/episodioUi'

import MensagemFlash from '../components/shared/mensagemFlash'
import MeuInputNumero from '../components/shared/meuInputNumero'
import MeuBotao from '../components/shared/meuBotao'
import TodosEpisodios from './todosEpisodios'
import listaAvaliacoes from './listaAvaliacoes';
import { Link } from 'react-router-dom'
import '../App.css';


export default class PaginaInicial extends Component {
  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    // definindo estado inicial de um componente
    this.state = {
      //episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }
    componentDidMount() {
      // esse setTimeout é para simular lentidão de 3 segs
      this.episodiosApi.buscar()
        .then( episodiosDoServidor => {
          this.listaEpisodios = new ListaEpisodios( episodiosDoServidor.data )
          setTimeout( () => {
            this.setState( { episodio: this.listaEpisodios.episodioAleatorio } )
          }, 1500 )
        } )
    }
  
    sortear = () => {
      const episodio = this.listaEpisodios.episodioAleatorio
      this.setState( {
        episodio, deveExibirMensagem: false
      } )
    }
  
    marcarComoAssistido = () => {
      const { episodio } = this.state
      this.listaEpisodios.marcarComoAssistido( episodio )
      this.setState( {
        episodio,
      } )
    }
      exibeMensagem = ( { cor, mensagem } ) => {
        this.setState( {
          cor, mensagem, deveExibirMensagem: true
        } )
      } 
    
      registrarNota = ( { valor, erro } ) => {
        if ( erro ) {
          // se tiver erro no campo, atualizamos estado com exibição e já retornamos,
          // para não rodar o código como se estivesse válida a obrigatoriedade
          return this.setState( {
            deveExibirErro: erro
          } )
        } else {
          this.setState( {
            deveExibirErro: erro
          } )
        }
        
        const { episodio } = this.state
        let cor, mensagem
        if ( episodio.validarNota( valor ) ) {
          episodio.avaliar( valor )
          // TODO: centralizar mensagens num arquivo
          cor = 'verde'
          mensagem = 'Registramos sua nota!'
        } else {
          // TODO: centralizar mensagens num arquivo
          cor = 'vermelho'
          mensagem = 'Informar uma nota válida (entre 1 e 5)'
        }
        this.exibeMensagem( { cor, mensagem } )
        
      }
    
      atualizarMensagem = devoExibir => {
        this.setState( {
          deveExibirMensagem: devoExibir
        } )
      }
     


      render() {
        const { episodio, deveExibirMensagem, mensagem, cor, deveExibirErro } = this.state
        const { listaEpisodios } = this
        return (
          !episodio ? (
            <h3>Aguarde...</h3>
          ) : (
            <React.Fragment>
              <MensagemFlash atualizarMensagem={ this.atualizarMensagem } cor={ cor } deveExibirMensagem={ deveExibirMensagem } mensagem={ mensagem } segundos={ 5 } />
              <EpisodioUi episodio={ episodio } />
              <MeuInputNumero placeholder="1 a 5"
                mensagemCampo="Qual sua nota para este episódio?"
                obrigatorio={ true }
                atualizarValor={ this.registrarNota }
                deveExibirErro={ deveExibirErro }
                visivel={ episodio.assistido || false }/>
              <div className="botoes">
                <MeuBotao cor="verde" quandoClicar={ this.sortear } texto="Próximo" />
                <MeuBotao cor="azul" quandoClicar={ this.marcarComoAssistido } texto="Já assisti"/> 
                <MeuBotao cor="vermelho" link="/avaliacoes"  dadosNavegacao={ { listaEpisodios } } texto="Ver notas!"/>
                <MeuBotao cor="roxo" link="/todosEps" dadosNavegacao={ { listaEpisodios } } texto="Lista de Episódios"/>
              </div>
            </React.Fragment>
          )
        )
      }
    }