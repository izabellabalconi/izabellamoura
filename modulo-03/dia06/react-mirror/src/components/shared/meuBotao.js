import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './meuBotao.css'

const MeuBotao = ( { cor, texto, quandoClicar, link, dadosNavegacao } ) =>
  <React.Fragment>
    <button className={ `btn ${ cor }` } onClick={ quandoClicar }>
      {
        link ? <Link to={{ pathname: link, state: dadosNavegacao }}>{ texto }</Link> : texto
      }
    </button>
  </React.Fragment>

MeuBotao.propTypes = {
  texto: PropTypes.string.isRequired,
  quandoClicar: PropTypes.func,
  link: PropTypes.string,
  dadosNavegacao: PropTypes.object,
  cor: PropTypes.oneOf( [ 'verde', 'azul', 'vermelho', 'roxo' ] ),
}

MeuBotao.defaultProps = {
  cor: 'verde'
}

export default MeuBotao