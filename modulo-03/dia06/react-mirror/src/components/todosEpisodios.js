import React from 'react'
import { Link } from 'react-router-dom'
import MeuBotao from '../components/shared/meuBotao'
import PaginaInicial from './paginaInicial'
import './todosEpisodios.css';


function ordemDefinitiva( a, b ) {
    const resultadoTemporario = a.temporada - b.temporada
    return resultadoTemporario === 0 ? a.ordemEpisodio - b.ordemEpisodio : resultadoTemporario
} 

const TodosEpisodios = props => {
  const { listaEpisodios } = props.location.state
  const temporadaParaOrdena = listaEpisodios.todos.sort( ordemDefinitiva )
  return (
    <React.Fragment>
      { temporadaParaOrdena.map( e => <Link className="link" to={{ pathname: "/episodio", state: listaEpisodios }} value={ e.id }><li key={ e.id }>{ `${ e.nome } -  ${ e.temporada } - ${ e.ordemEpisodio }` }</li></Link> ) }
      <MeuBotao cor="azul" link="/" exact component={ PaginaInicial } texto="voltar" />
      <MeuBotao cor="verde" link="/" exact component={ PaginaInicial } texto="Ordenar por data de estreia" />
      <MeuBotao cor="verde" link="/" exact component={ PaginaInicial } texto="Ordenar por duração" />
    </React.Fragment>
    )
}

export default TodosEpisodios
