import axios from 'axios'

export default class EpisodiosApi {
  buscar() {
    return axios.get( 'http://localhost:9000/api/episodios' )
  }
  registrarNota( { nota, episodioId } ) {
    return axios.post( 'http://localhost:9000/api/notas', { nota, episodioId } )
  }
  buscarNotas(){
    return axios.get( 'http://localhost:/api/notas' )
  }
  buscarDetalhes( id ){
    return axios.get( `${ 'http://localhost:9000/api/'}/episodios/${ id }/detalhes `)

  }
} 


