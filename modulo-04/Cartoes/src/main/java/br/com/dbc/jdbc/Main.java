/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author izabella.balconi
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Connection conn = Connector.connect();
        
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'CREDENCIADOR'").executeQuery();
            if(!rs.next()) {
            conn.prepareStatement("CREATE TABLE USUARIO (\n"
                    + "  ID NUMBER NOT NULL PRIMARY KEY,\n"
                    + "  NOME VARCHAR(100) NOT NULL,\n"
                    
                    + ")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into usuario(id,nome" + 
                    "values (usuario_seq.nextval,?,?,?,?)");
            pst.setString(1, "Credenciador 1");
           
            pst.executeUpdate();
            rs= conn.prepareStatement("select * from credenciador").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Id do Credenciador: %s", rs.getInt("id")));
                System.out.println(String.format("Nome do Credenciador: %s", rs.getString("nome")));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na consulta do Main", ex);
        }
        
    }

}
