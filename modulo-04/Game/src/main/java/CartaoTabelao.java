
import com.oracle.webservices.internal.api.EnvelopeStyle;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author izabella.balconi
 */
@Entity
 @Table(name = "CARTAO_TABELAO")
 @Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo_cartao",
                 discriminatorType = DiscriminatorType.STRING)
        
public class CartaoTabelao {
     @Id
     @SequenceGenerator(allocationSize = 1 , name = "CARTAO_TABELAO_SEQ",
                sequenceName = "CARTAO_TABELAO_SEQ")
     @GeneratedValue(generator = "CARTAO_TABELAO_SEQ", strategy = GenerationType.SEQUENCE)
     private Integer id;
    
     private int diaVencimento;
     
}