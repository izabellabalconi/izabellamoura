/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.dao;

import br.com.dbc.jogos.entity.Agencia;
import br.com.dbc.jogos.entity.Cliente;
import java.util.Optional;

/**
 *
 * @author izabella.balconi
 */
public class AgenciaDAO extends AbstractDAO<Agencia> {

    @Override
    protected Class<Agencia> getEntityClass(){
        return Agencia.class;
    } 
        
}
