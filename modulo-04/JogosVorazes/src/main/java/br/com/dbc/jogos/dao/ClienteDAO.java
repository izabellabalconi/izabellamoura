/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.dao;

import br.com.dbc.jogos.dto.ClienteDTO;
import br.com.dbc.jogos.entity.Cliente;
import java.util.Optional;

/**
 *
 * @author tiago
 */
public class ClienteDAO extends AbstractDAO<Cliente> {

    @Override
    protected Class<Cliente> getEntityClass() {
        return Cliente.class;
    }

    public Cliente from(ClienteDTO dto) {
        Cliente c = Optional
                .ofNullable(findById(dto.getIdCliente()))
                .orElseGet(() -> new Cliente());
        c.setNome(dto.getNomeCliente());
        c.setCpf(dto.getCpfCliente());
        return c;
    }

}
