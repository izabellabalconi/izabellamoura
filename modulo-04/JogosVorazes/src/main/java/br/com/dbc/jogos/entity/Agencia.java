/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author izabella.balconi
 */
@Entity
public class Agencia extends BaseEntity{
    
     @Id
    @SequenceGenerator(allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")
    @GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
}
