/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.service;

import br.com.dbc.jogos.dao.AgenciaDAO;
import br.com.dbc.jogos.dao.ClienteDAO;
import br.com.dbc.jogos.entity.Agencia;
import java.util.logging.Logger;

/**
 *
 * @author izabella.balconi
 */
public class AgenciaService  {
    
    private static final AgenciaDAO AGENCIA_DAO = new AgenciaDAO();
    
    
    public Agencia buscarPorId( Integer id ) {
        return AGENCIA_DAO.findById(id);
    
    }
    
}
