/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.service;

import br.com.dbc.jogos.dao.ClienteDAO;
import br.com.dbc.jogos.dto.ClienteDTO;
import br.com.dbc.jogos.entity.Cliente;
import br.com.dbc.jogos.entity.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 *
 * @author tiago
 */
public class ClienteService {

    private static final Logger LOG = Logger.getLogger(ClienteService.class.getName());
    private static final ClienteDAO CLIENTE_DAO = new ClienteDAO();
    private static final EnderecoService ENDERECO_SERVICE = new EnderecoService();

    public void salvarCliente(ClienteDTO dto) {
        Session session = HibernateUtil.getSession(true);
        try {
            Cliente cliente = CLIENTE_DAO.from(dto);
            CLIENTE_DAO.saveOrUpdate(cliente);

            ENDERECO_SERVICE.salvarEndereco(dto.getEnderecoDTO(), cliente);

            HibernateUtil.commitTransaction();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            session.getTransaction().rollback();
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
