package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.service.EnderecoDTO;
import br.com.dbc.lotr.service.PersonagemDTO;
import br.com.dbc.lotr.service.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        dto.setApelidoUsuario("Gustavo");
        dto.setNomeUsuario("Gustavo");
        dto.setCpfUsuario(123l);
        dto.setSenhaUsuario("123");
        
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setLogradouro("rua do gustavo");
        enderecoDTO.setNumero(123);
        enderecoDTO.setBairro("bairro do gustavo");
        enderecoDTO.setCidade("cidade do gustavo");
        enderecoDTO.setComplemento("cep 123");
        dto.setEndereco(enderecoDTO);
        
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Elfo do gustavo");
        personagemDTO.setDanoElfo(123d);
        dto.setPersonagem(personagemDTO);
        usuarioService.cadastrarUsuarioEPersonagem(dto);
        System.exit(0);
        
    }
    public static void oldmain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            Usuario usuario = new Usuario();
            usuario.setNome("Antônio");
            usuario.setApelido("antonio");
            usuario.setCpf(123l);
            usuario.setSenha("456");
            
            Endereco endereco = new Endereco();
            endereco.setLogradouro("rua do antonio");
            endereco.setNumero(100);
            endereco.setBairro("bairro do antonio");
            endereco.setComplemento("ape antonio");
            endereco.setCidade("cidade do antonio");
            
            Endereco endereco2 = new Endereco();
            endereco2.setLogradouro("rua do antonio");
            endereco2.setNumero(150);
            endereco2.setBairro("bairro do antonio");
            endereco2.setComplemento("ape antonio");
            endereco2.setCidade("cidade do antonio");
            
            TipoContato tipoContato = new TipoContato();
            tipoContato.setNome("celular");
            tipoContato.setQuantidade(5);
            
            Contato contato = new Contato();
            contato.setTipoContato(tipoContato);
            contato.setUsuario(usuario);
            contato.setValor("99888926");
            
            tipoContato.setContato(contato);
            usuario.pushContatos(contato);
            usuario.pushEnderecos(endereco, endereco2);
            
            
            session.save(usuario);
            
            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d);
            elfoTabelao.setNome("Legolas");
            session.save(elfoTabelao);
            
            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(10d);
            hobbitTabelao.setNome("Frodo");
            session.save(hobbitTabelao);
            
            ElfoPerClass elfo = new ElfoPerClass();
            elfo.setNome("Legolas Per Class");
            elfo.setDanoElfo(100d);
            session.save(elfo);
            
            HobbitPerClass hobbit = new HobbitPerClass();
            hobbit.setNome("Frodo Per Class");
            hobbit.setDanoHobbit(10d);
            session.save(hobbit);
            
            ElfoJoin elfojoin = new ElfoJoin();
            elfojoin.setNome("Legolas Join");
            elfojoin.setDanoElfo(100d);
            session.save(elfojoin);
            
            HobbitJoin hobbitjoin = new HobbitJoin();
            hobbitjoin.setNome("Frodo Join");
            hobbitjoin.setDanoHobbit(10d);
            session.save(hobbitjoin);
            
             Criteria criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%do antonio"),
                            Restrictions.ilike("endereco.cidade", "%do antonio")
                    ));

            List<Usuario> usuarios = criteria.list();

            System.out.println(usuarios);
            usuarios.forEach(System.out::println);

            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%do antonio"),
                            Restrictions.ilike("endereco.cidade", "%do antonio")
                    ));

            criteria.setProjection(Projections.count("id"));
            System.out
                    .println(String
                            .format("Foram encontrados %s registro(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));
            
            usuarios = session.createQuery("select u from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antonio' "
                    + " and lower(endereco.bairro) like '%do antonio' ")
                    .list();
            
            usuarios.forEach(System.out::println);
            
            //contagem de usuario com HQL
            Long count = (Long)session
                    .createQuery("select count(distinct u.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antonio' "
                    + " and lower(endereco.bairro) like '%do antonio' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s usuarios", count));
           
            //contagem de usuario com HQL
            count = (Long)session
                    .createQuery("select count(endereco.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antonio' "
                    + " and lower(endereco.bairro) like '%do antonio' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s valores", count));
            
            
            //Conta quantidade de Linhas
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%do antonio"),
                            Restrictions.ilike("endereco.cidade", "%do antonio")
                    ));
            criteria.setProjection(Projections.rowCount());
             System.out
                    .println(String
                            .format("Foram encontrados %s registro(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));
            //conta contagem de enderecos
             criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions
                    .and(
                            Restrictions.ilike("endereco.bairro", "%do antonio"),
                            Restrictions.ilike("endereco.cidade", "%do antonio")
                    ));
             criteria.setProjection(Projections.count("enderecos"));
             System.out
                    .println(String
                            .format("Foram encontrados %s registro(s) "
                                    + "com os critérios especificados",
                                    criteria.uniqueResult()));
            
            
            
           
            transaction.commit();
        } catch (Exception ex) {
            if(transaction != null)
            transaction.rollback();
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
                System.exit(1);
        } finally {
            if(session != null)
            session.close();
            System.exit(0);
        }
    }

}
