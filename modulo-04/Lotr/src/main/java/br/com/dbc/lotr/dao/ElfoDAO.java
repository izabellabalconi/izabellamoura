/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.service.PersonagemDTO;
import br.com.dbc.lotr.service.UsuarioPersonagemDTO;
import java.util.List;

/**
 *
 * @author izabella.balconi
 */
public class ElfoDAO extends AbstractDAO <ElfoJoin> {
    

    public ElfoJoin parseFROM(PersonagemDTO personagemDTO, Usuario usuario) {
         ElfoJoin elfoEntity = new ElfoJoin();
         
         if(personagemDTO.getId() != null) {
             elfoEntity = buscar(personagemDTO.getId());
        
         }
             if(elfoEntity == null) {
                elfoEntity= new ElfoJoin();
            }
             elfoEntity.setUsuario(usuario);
             elfoEntity.setDanoElfo(personagemDTO.getDanoElfo());
             elfoEntity.setNome(personagemDTO.getNome());
             
             return elfoEntity;
   
    }

    @Override
    protected Class<ElfoJoin> getEntityClass() {
         return ElfoJoin.class;
    }
     
    
}
       
    
    
    

