/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.service.PersonagemDTO;
import br.com.dbc.lotr.service.UsuarioPersonagemDTO;
import java.util.List;

/**
 *
 * @author izabella.balconi
 */
public class HobbitDAO extends AbstractDAO <HobbitJoin>{

    public HobbitJoin parseFrom(PersonagemDTO personagemDTO, Usuario usuario) {
        
           HobbitJoin hobbitEntity = new HobbitJoin();
         
         if(personagemDTO.getId() != null) 
             hobbitEntity = buscar(personagemDTO.getId());
        
         
             if(hobbitEntity == null) 
                hobbitEntity= new HobbitJoin();
            
             hobbitEntity.setUsuario(usuario);
             hobbitEntity.setDanoHobbit(personagemDTO.getDanoHobbit());
             hobbitEntity.setNome(personagemDTO.getNome());
             
             return hobbitEntity;
   
    }

    @Override
    protected Class<HobbitJoin> getEntityClass() {
        return HobbitJoin.class;
    }
}
