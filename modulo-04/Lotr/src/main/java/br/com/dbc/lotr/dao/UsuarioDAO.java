/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.service.EnderecoDTO;
import br.com.dbc.lotr.service.UsuarioPersonagemDTO;
import java.util.List;

/**
 *
 * @author izabella.balconi
 */
public class UsuarioDAO extends AbstractDAO <Usuario>{
    
    public Usuario parseFrom(UsuarioPersonagemDTO dto){
        Usuario usuarioEntity = null;
        if(dto.getIdUsuario() != null){
            usuarioEntity = buscar(dto.getIdUsuario());
        }
              
        if(usuarioEntity == null){
            usuarioEntity = new Usuario();
        }
        usuarioEntity.setNome(dto.getNomeUsuario());
        usuarioEntity.setApelido(dto.getApelidoUsuario());
        usuarioEntity.setSenha(dto.getSenhaUsuario());
        usuarioEntity.setCpf(dto.getCpfUsuario());
        
        //Endereço
        EnderecoDTO enderecoDTO = dto.getEndereco();
                Endereco enderecoEntity = new Endereco();
            if(enderecoDTO.getId() != null) {
                enderecoEntity.setId(enderecoDTO.getId());
                int indexEndereco = usuarioEntity.getEnderecos().indexOf(enderecoEntity);
                    if(indexEndereco >= 0) {
                        enderecoEntity = usuarioEntity.getEnderecos().get(indexEndereco);
                }
            
            }
            enderecoEntity.setLogradouro(enderecoDTO.getLogradouro());
            enderecoEntity.setNumero(enderecoDTO.getNumero());
            enderecoEntity.setBairro(enderecoDTO.getBairro());
            enderecoEntity.setCidade(enderecoDTO.getCidade());
            enderecoEntity.setComplemento(enderecoDTO.getComplemento());
            
            if(!usuarioEntity.getEnderecos().contains(enderecoEntity)) {
                usuarioEntity.getEnderecos().add(enderecoEntity);
            }
            
            if(!enderecoEntity.getUsuarios().contains(usuarioEntity)){
                enderecoEntity.pushUsuarios(usuarioEntity);
            }
            
            return usuarioEntity;
    }   

    @Override
    protected Class<Usuario> getEntityClass() {
        return Usuario.class;
    }
}
