/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PERSONAGEM_JOIN")
@Inheritance(strategy = InheritanceType.JOINED)
public class PersonagemJoin extends AbstractEntity {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_JOIN_SEQ",
            sequenceName = "PERSONAGEM_JOIN_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_JOIN_SEQ",
            strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    private String nome;
    @Enumerated(EnumType.STRING)
    private RacaType raca;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public RacaType getRaca() {
        return raca;
    }

    protected void setRaca(RacaType raca) {
        this.raca = raca;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
