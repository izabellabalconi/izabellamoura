/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;


@MappedSuperclass
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
 

public class PersonagemPerClass {
    @Id
     @SequenceGenerator(allocationSize = 1 , name = "PERSONAGEM_CLASS_SEQ",
                sequenceName = "PERSONAGEM_CLASS_SEQ")
     @GeneratedValue(generator = "PERSONAGEM_CLASS_SEQ",
             strategy = GenerationType.SEQUENCE)
    
     private Integer id;
    
     private String nome;
     
     @Enumerated (EnumType.STRING)
     private RacaType raca;

    public RacaType getRaca() {
        return raca;
    }

    protected void setRaca(RacaType raca) {
        this.raca = raca;
    }

     
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
