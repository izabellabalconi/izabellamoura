/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.service;

import br.com.dbc.lotr.dao.ElfoDAO;
import br.com.dbc.lotr.dao.HobbitDAO;
import br.com.dbc.lotr.dao.UsuarioDAO;
import br.com.dbc.lotr.entity.HibernateUtil;
import static br.com.dbc.lotr.entity.RacaType.ELFO;
import static br.com.dbc.lotr.entity.RacaType.HOBBIT;
import br.com.dbc.lotr.entity.Usuario;
import java.util.logging.Level;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.logging.Logger;


/**
 *
 * @author izabella.balconi
 */
public class UsuarioService {
    // dto- DATA TRANSFER OBJECT = NAO FAZ NADA SO VAI TER ATRIBUTOS RECEBIDOS DO FRONT END.
    
    private static final UsuarioDAO USUARIO_DAO = new UsuarioDAO();
    private static final PersonagemService PERSONAGEM_SERVICE = new PersonagemService();
    
    
    public void cadastrarUsuarioEPersonagem(
            UsuarioPersonagemDTO dto){
        boolean started = HibernateUtil.beginTransaction();
        Transaction transaction = HibernateUtil.getSession().getTransaction();
                
        try{
        Usuario usuario = USUARIO_DAO.parseFrom(dto);
        USUARIO_DAO.criar(usuario);
    
        PERSONAGEM_SERVICE.salvarPersonagem(dto.getPersonagem(), usuario);
        
        if(started){
            transaction.commit();
            }
        }
        catch(Exception e) {
            transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(),e );
         }
     
}
    private static final Logger LOG = Logger.getLogger(UsuarioService.class.getName());
  
}