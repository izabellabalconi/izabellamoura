/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.service;

import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.PersonagemJoin;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.Usuario;
import java.util.List;
import junit.framework.Assert;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author izabella.balconi
 */
public class PersonagemServiceTest {
    
    public PersonagemServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of salvarPersonagem method, of class PersonagemService.
     */
    @Test
    public void testSalvarPersonagem() {
        System.out.println("salvarPersonagem");
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Elfo da lua");
        personagemDTO.setDanoElfo(123d);
        
        Usuario usuario = new Usuario();
        usuario.setNome("user elfico");
        usuario.setApelido("elfinho");
        usuario.setCpf(123l);
        usuario.setSenha("123");
        
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();
        
        
        PersonagemService instance = new PersonagemService();
        instance.salvarPersonagem(personagemDTO, usuario);        
        
        final List personagens = session.createCriteria(PersonagemJoin.class).list();
        Assert.assertEquals(1, personagens.size());
        Assert.assertEquals("elfinho", usuario.getApelido());
    }
    
}
