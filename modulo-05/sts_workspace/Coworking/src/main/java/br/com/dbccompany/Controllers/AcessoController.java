package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.Acesso;
import br.com.dbccompany.Services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Acesso> listarTodos() {
        return acessoService.buscarTodos();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Acesso buscarPorID(@PathVariable Integer id) {
        return acessoService.buscarPorID(id);
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public Acesso salvarNovo(@RequestBody Acesso acesso) {
        return acessoService.salvar(acesso);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Acesso editar(@PathVariable Integer id, @RequestBody Acesso acesso) {
        return acessoService.editar(id, acesso);
    }
}
