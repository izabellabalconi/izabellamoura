package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {
	
	@Autowired
    ClienteService clienteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Cliente> listarTodos() {
		return clienteService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Cliente buscarPorID(@PathVariable Integer id) {
		return clienteService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Cliente salvarNovo(@RequestBody Cliente cliente) {
		return clienteService.salvar(cliente);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Cliente editar(@PathVariable Integer id, @RequestBody Cliente cliente) {
		return clienteService.editar(id, cliente);
	}
}