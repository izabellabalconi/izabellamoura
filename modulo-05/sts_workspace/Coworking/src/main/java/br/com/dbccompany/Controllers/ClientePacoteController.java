package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Services.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientepacote")
public class ClientePacoteController {
	
	@Autowired
    ClientePacoteService clientePacoteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<ClientePacote> listarTodos() {
		return clientePacoteService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public ClientePacote buscarPorID(@PathVariable Integer id) {
		return clientePacoteService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public ClientePacote salvarNovo(@RequestBody ClientePacote clientePacote) {
		return clientePacoteService.salvar(clientePacote);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public ClientePacote editar(@PathVariable Integer id, @RequestBody ClientePacote clientePacote) {
		return clientePacoteService.editar(id, clientePacote);
	}
}
