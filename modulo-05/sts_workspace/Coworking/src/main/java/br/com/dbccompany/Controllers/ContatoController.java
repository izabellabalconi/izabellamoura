package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {
	
	@Autowired
    ContatoService contatoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Contato> listarTodos() {
		return contatoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Contato buscarPorID(@PathVariable Integer id) {
		return contatoService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Contato salvarNovo(@RequestBody Contato contato) {
		return contatoService.salvar(contato);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Contato editar(@PathVariable Integer id, @RequestBody Contato contato) {
		return contatoService.editar(id, contato);
	}

}