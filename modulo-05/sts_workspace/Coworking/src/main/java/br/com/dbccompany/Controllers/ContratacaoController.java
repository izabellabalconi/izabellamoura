package br.com.dbccompany.Controllers;


import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Services.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {
	
	@Autowired
    ContratacaoService contratacaoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Contratacao> listarTodos() {
		return contratacaoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Contratacao buscarPorID(@PathVariable Integer id) {
		return contratacaoService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Contratacao salvarNovo(@RequestBody Contratacao contratacao) {
		return contratacaoService.salvar(contratacao);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Contratacao editar(@PathVariable Integer id, @RequestBody Contratacao contratacao) {
		return contratacaoService.editar(id, contratacao);
	}
}