package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Services.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {
	
	@Autowired
    EspacoService espacoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Espaco> listarTodos() {
		return espacoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Espaco buscarPorID(@PathVariable Integer id) {
		return espacoService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Espaco salvarNovo(@RequestBody Espaco espaco) {
		return espacoService.salvar(espaco);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Espaco editar(@PathVariable Integer id, @RequestBody Espaco espaco) {
		return espacoService.editar(id, espaco);
	}
}