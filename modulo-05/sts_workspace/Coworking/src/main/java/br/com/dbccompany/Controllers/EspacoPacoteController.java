package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Services.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacopacote")
public class EspacoPacoteController {
	
	@Autowired
    EspacoPacoteService espacoPacoteService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<EspacoPacote> listarTodos() {
		return espacoPacoteService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public EspacoPacote buscarPorID(@PathVariable Integer id) {
		return espacoPacoteService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public EspacoPacote salvarNovo(@RequestBody EspacoPacote espacoPacote) {
		return espacoPacoteService.salvar(espacoPacote);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public EspacoPacote editar(@PathVariable Integer id, @RequestBody EspacoPacote espacoPacote) {
		return espacoPacoteService.editar(id, espacoPacote);
	}
}