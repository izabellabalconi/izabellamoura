package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.Pagamento;
import br.com.dbccompany.Services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {
	
	@Autowired
    PagamentoService pagamentoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Pagamento> listarTodos() {
		return pagamentoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Pagamento buscarPorID(@PathVariable Integer id) {
		return pagamentoService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Pagamento salvarNovo(@RequestBody Pagamento pagamento) {
		return pagamentoService.salvar(pagamento);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Pagamento editar(@PathVariable Integer id, @RequestBody Pagamento pagamento) {
		return pagamentoService.editar(id, pagamento);
	}
}