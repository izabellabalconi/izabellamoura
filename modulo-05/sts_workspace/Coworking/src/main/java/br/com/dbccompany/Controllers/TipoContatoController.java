package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Services.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipocontato")
public class TipoContatoController {
	
	@Autowired
    TipoContatoService tipoContatoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<TipoContato> listarTodos() {
		return tipoContatoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public TipoContato buscarPorID(@PathVariable Integer id) {
		return tipoContatoService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public TipoContato salvarNovo(@RequestBody TipoContato tipoContato) {
		return tipoContatoService.salvar(tipoContato);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public TipoContato editar(@PathVariable Integer id, @RequestBody TipoContato tipoContato) {
		return tipoContatoService.editar(id, tipoContato);
	}
}