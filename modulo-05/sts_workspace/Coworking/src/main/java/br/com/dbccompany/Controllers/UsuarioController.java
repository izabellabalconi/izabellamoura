package br.com.dbccompany.Controllers;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {
	
	@Autowired
    UsuarioService usuarioService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Usuario> listarTodos() {
		return usuarioService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Usuario buscarPorID(@PathVariable Integer id) {
		return usuarioService.buscarPorID(id);
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Usuario salvarNovo(@RequestBody Usuario usuario) {
		return usuarioService.salvar(usuario);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Usuario editar(@PathVariable Integer id, @RequestBody Usuario usuario) {
		return usuarioService.editar(id, usuario);
	}
}