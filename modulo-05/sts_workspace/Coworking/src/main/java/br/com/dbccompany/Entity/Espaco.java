package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "ESPACO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Espaco {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @Column(nullable = false, name = "QTD_PESSOAS")
    private Integer qtdPessoas;

    @Column(nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "espaco")
    private List<EspacoPacote> espacoPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco")
    private List<Contratacao> contratacoes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacoPacote> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void pushEspacoPacotes(EspacoPacote... espacoPacotes) {
        this.espacoPacotes.addAll(Arrays.asList(espacoPacotes));
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void pushContratacoes(Contratacao... contratacoes) {
        this.contratacoes.addAll(Arrays.asList(contratacoes));
    }
}