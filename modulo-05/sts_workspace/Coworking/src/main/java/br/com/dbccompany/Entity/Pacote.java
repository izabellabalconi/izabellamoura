package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "PACOTE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Pacote {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "pacote")
    private List<EspacoPacote> espacoPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacote")
    private List<ClientePacote> clientePacotes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacoPacote> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void pushEspacoPacotes(EspacoPacote... espacoPacotes) {
        this.espacoPacotes.addAll(Arrays.asList(espacoPacotes));
    }

    public List<ClientePacote> getClientePacotes() {
        return clientePacotes;
    }

    public void pushClientePacotes(ClientePacote... clientePacotes) {
        this.clientePacotes.addAll(Arrays.asList(clientePacotes));
    }
}
