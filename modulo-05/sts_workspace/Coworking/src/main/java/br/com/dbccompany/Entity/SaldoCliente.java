package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class SaldoCliente {

    @EmbeddedId
    private SaldoClienteId id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "TIPO_CONTRATACAO")
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private Integer vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    private List<Acesso> acesso = new ArrayList<>();

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getVencimento() {
        return vencimento;
    }

    public void setVencimento(Integer vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acesso> getAcesso() {
        return acesso;
    }

    public void pushAcesso(Acesso... acesso) {
        this.acesso.addAll(Arrays.asList(acesso));
    }
}

