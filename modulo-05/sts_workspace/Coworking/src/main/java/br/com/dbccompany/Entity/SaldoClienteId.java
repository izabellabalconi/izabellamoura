package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {

    @Column(name = "ID_CLIENTE")
    private Integer idCliente;

    @Column(name = "ID_ESPACO")
    private Integer idEspaco;

    public SaldoClienteId() {}

    public SaldoClienteId(Integer idCliente, Integer idEspaco) {
        this.idCliente = idCliente;
        this.idEspaco = idEspaco;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public Integer getIdEspaco() {
        return idEspaco;
    }

   

}