package br.com.dbccompany.Entity;

public enum TipoContratacao {

    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES;

}
