package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Acesso;

import java.sql.Date;
import java.util.List;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {
    List<Acesso> findAllByData(Date data);
}