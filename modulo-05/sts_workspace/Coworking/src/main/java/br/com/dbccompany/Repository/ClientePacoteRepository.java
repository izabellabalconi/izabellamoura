package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Integer> {
	List<ClientePacote> findAllByCliente(Cliente cliente);
	List<ClientePacote> findAllByPacote(Pacote pacote);
}
