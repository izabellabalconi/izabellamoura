package br.com.dbccompany.Repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	Cliente findByNome(String nome);
	Cliente findByCpf(String cpf);
	Cliente findByDataNascimento(Date data);
}
