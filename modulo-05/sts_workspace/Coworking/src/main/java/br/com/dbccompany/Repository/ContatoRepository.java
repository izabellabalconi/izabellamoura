package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer>{
	List<Contato> findAllByTipoContato(TipoContato tipoContato);
	List<Contato> findAllByValor(Double valor);
	List<Contato> findAllByCliente(Cliente cliente);
}