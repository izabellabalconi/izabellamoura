package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.TipoContratacao;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer>{
	List<Contratacao> findAllByEspaco(Espaco espaco);
	List<Contratacao> findAllByCliente(Cliente cliente);
	List<Contratacao> findAllByTipoContratacao(TipoContratacao tipoContratacao);
}
