package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Integer>{
	List<EspacoPacote> findAllByTipoContratacao(TipoContratacao tipoContratacao);
	List<EspacoPacote> findAllByEspaco(Espaco espaco);
	List<EspacoPacote> findAllByPacote(Pacote pacote);
}
