package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Espaco;

public interface EspacoRepository extends CrudRepository<Espaco, Integer>{
	Espaco findByNome(String nome);
	List<Espaco> findAllByQtdPessoas(Integer qtdPessoas);
	List<Espaco> findAllByValor(Double valor);
}
