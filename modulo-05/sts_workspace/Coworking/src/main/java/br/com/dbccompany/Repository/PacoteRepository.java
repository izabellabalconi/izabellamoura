package br.com.dbccompany.Repository;


import br.com.dbccompany.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PacoteRepository extends CrudRepository<Pacote, Integer>{
	List<Pacote> findByValor(Double valor);
}