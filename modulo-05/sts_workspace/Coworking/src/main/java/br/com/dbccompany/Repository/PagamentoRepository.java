package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Pagamento;
import br.com.dbccompany.Entity.TipoPagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer>{
	List<Pagamento> findAllByClientePacote(ClientePacote clientePacote);
	List<Pagamento> findAllByContratacao(Contratacao contratacao);
	List<Pagamento> findAllByTipoPagamento(TipoPagamento tipoPagamento);
}

