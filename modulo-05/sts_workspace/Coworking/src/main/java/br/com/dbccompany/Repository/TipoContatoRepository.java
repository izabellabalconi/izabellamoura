package br.com.dbccompany.Repository;


import br.com.dbccompany.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

public interface TipoContatoRepository extends CrudRepository<TipoContato, Integer>{
	TipoContato findByNome(String nome);
}