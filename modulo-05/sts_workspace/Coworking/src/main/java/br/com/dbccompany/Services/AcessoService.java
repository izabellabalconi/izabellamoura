package br.com.dbccompany.Services;
import br.com.dbccompany.Entity.Acesso;
import br.com.dbccompany.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AcessoService {

    @Autowired
    public AcessoRepository acessoRepository;

    @Transactional(rollbackFor = Exception.class)
    public Acesso salvar(Acesso acesso) {
        return acessoRepository.save(acesso);
    }

    public Acesso buscarPorID(Integer id) {
        if( acessoRepository.findById(id).isPresent() )
            return acessoRepository.findById(id).get();
        return null;
    }

    public List<Acesso> buscarTodos() {
        return (List<Acesso>) acessoRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Acesso editar(Integer id, Acesso acesso) {
        acesso.setId(id);
        return acessoRepository.save(acesso);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Acesso acesso) {
        acessoRepository.delete(acesso);
    }
}
