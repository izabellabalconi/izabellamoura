package br.com.dbccompany.Services;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteService {

	@Autowired
	public ClienteRepository clienteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente salvar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	public Cliente buscarPorID(Integer id) {
		if( clienteRepository.findById(id).isPresent() )
			return clienteRepository.findById(id).get();
		return null;
	}
	
	public List<Cliente> buscarTodos() {
		return (List<Cliente>) clienteRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente editar(Integer id, Cliente cliente) {
		cliente.setId(id);
		return clienteRepository.save(cliente);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Cliente cliente) {
		clienteRepository.delete(cliente);
	}
	
}
