package br.com.dbccompany.Services;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatoService {
	
	@Autowired
	public ContatoRepository contatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Contato salvar(Contato contato) {
		return contatoRepository.save(contato);
	}
	
	public Contato buscarPorID(Integer id) {
		if( contatoRepository.findById(id).isPresent() )
			return contatoRepository.findById(id).get();
		return null;
	}
	
	public List<Contato> buscarTodos() {
		return (List<Contato>) contatoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Contato editar(Integer id, Contato contato) {
		contato.setId(id);
		return contatoRepository.save(contato);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Contato contato) {
		contatoRepository.delete(contato);
	}
	
}