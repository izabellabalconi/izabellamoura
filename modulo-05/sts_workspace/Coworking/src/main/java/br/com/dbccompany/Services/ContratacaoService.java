package br.com.dbccompany.Services;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.TipoContratacao;
import br.com.dbccompany.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContratacaoService {

	@Autowired
	public ContratacaoRepository contratacaoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Contratacao salvar(Contratacao contratacao) {
		return contratacaoRepository.save(contratacao);
	}
	
	public Contratacao buscarPorID(Integer id) {
		if( contratacaoRepository.findById(id).isPresent() )
			return contratacaoRepository.findById(id).get();
		return null;
	}
	
	public List<Contratacao> buscarTodos() {
		return (List<Contratacao>) contratacaoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Contratacao editar(Integer id, Contratacao contratacao) {
		contratacao.setId(id);
		return contratacaoRepository.save(contratacao);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Contratacao contratacao) {
		contratacaoRepository.delete(contratacao);
	}
	public Double calcularContratacao(Double valor, int quantidade, TipoContratacao tipoContratacao) {
        Double custoContratacao;
        switch (tipoContratacao) {
            case MINUTO:
                custoContratacao = valor * quantidade / 60;
                break;
            case HORA:
                custoContratacao = valor * quantidade;
                break;
            case TURNO:
                custoContratacao = valor * quantidade * 5;
                break;
            case DIARIA:
                custoContratacao = valor * quantidade * 8;
                break;
            case SEMANA:
                custoContratacao = valor * quantidade * 44;
                break;
            case MES:
                custoContratacao = valor * quantidade * 220;
                break;
            default:
                custoContratacao = valor * quantidade;
        }
        return custoContratacao;
    }
}

	
