package br.com.dbccompany.Services;

import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacoPacoteService {

	@Autowired
	public EspacoPacoteRepository espacoPacoteRepository;

	@Transactional(rollbackFor = Exception.class)
	public EspacoPacote salvar(EspacoPacote espacoPacote) {
		return espacoPacoteRepository.save(espacoPacote);
	}
	
	public EspacoPacote buscarPorID(Integer id) {
		if( espacoPacoteRepository.findById(id).isPresent() )
			return espacoPacoteRepository.findById(id).get();
		return null;
	}
	
	public List<EspacoPacote> buscarTodos() {
		return (List<EspacoPacote>) espacoPacoteRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public EspacoPacote editar(Integer id, EspacoPacote espacoPacote) {
		espacoPacote.setId(id);
		return espacoPacoteRepository.save(espacoPacote);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(EspacoPacote espacoPacote) {
		espacoPacoteRepository.delete(espacoPacote);
	}
	
}