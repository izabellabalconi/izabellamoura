package br.com.dbccompany.Services;

import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PacoteService {

	@Autowired
	public PacoteRepository pacoteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Pacote salvar(Pacote pacote) {
		return pacoteRepository.save(pacote);
	}
	
	public Pacote buscarPorID(Integer id) {
		if( pacoteRepository.findById(id).isPresent() )
			return pacoteRepository.findById(id).get();
		return null;
	}
	
	public List<Pacote> buscarTodos() {
		return (List<Pacote>) pacoteRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pacote editar(Integer id, Pacote pacote) {
		pacote.setId(id);
		return pacoteRepository.save(pacote);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Pacote pacote) {
		pacoteRepository.delete(pacote);
	}
	
}