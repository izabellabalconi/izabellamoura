package br.com.dbccompany.Services;
import br.com.dbccompany.Entity.Pagamento;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoClienteId;
import br.com.dbccompany.Repository.PagamentoRepository;
import br.com.dbccompany.Repository.SaldoClienteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PagamentoService {

	@Autowired
	public PagamentoRepository pagamentoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Pagamento salvar(Pagamento pagamento) {
		return pagamentoRepository.save(pagamento);
	}
	
	public Pagamento buscarPorID(Integer id) {
		if( pagamentoRepository.findById(id).isPresent() )
			return pagamentoRepository.findById(id).get();
		return null;
	}
	
	public List<Pagamento> buscarTodos() {
		return (List<Pagamento>) pagamentoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pagamento editar(Integer id, Pagamento pagamento) {
		pagamento.setId(id);
		return pagamentoRepository.save(pagamento);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Pagamento pagamento) {
		pagamentoRepository.delete(pagamento);
	}
	@Autowired
	public SaldoClienteRepository saldoClienteRepository;

	public void gerarSaldo(int idCliente, Espaco espaco) {
	     SaldoCliente saldoCliente;
	     SaldoClienteId id = new SaldoClienteId(idCliente, espaco.getId());
	       if (saldoClienteRepository.existsById(id)) {
	            saldoCliente = saldoClienteRepository.findById(id).get();
	            
	     } else {
	            saldoCliente = new SaldoCliente();
	            saldoCliente.setId(id);	            
	     }
	      saldoClienteRepository.save(saldoCliente);
	    }
	
}