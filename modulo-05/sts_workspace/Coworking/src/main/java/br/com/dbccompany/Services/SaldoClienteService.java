package br.com.dbccompany.Services;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoClienteId;
import br.com.dbccompany.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    public SaldoClienteRepository saldoClienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente) {
        return saldoClienteRepository.save(saldoCliente);
    }

    public SaldoCliente buscarPorID(SaldoClienteId id) {
        if( saldoClienteRepository.findById(id).isPresent() )
            return saldoClienteRepository.findById(id).get();
        return null;
    }

    public List<SaldoCliente> buscarTodos() {
        return (List<SaldoCliente>) saldoClienteRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editar(SaldoClienteId id, SaldoCliente saldoCliente) {
        saldoCliente.setId(id);
        return saldoClienteRepository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(SaldoCliente saldoCliente) {
        saldoClienteRepository.delete(saldoCliente);
    }
}
