package br.com.dbccompany.Services;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContatoService {

	@Autowired
	public TipoContatoRepository tipoContatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public TipoContato salvar(TipoContato tipoContato) {
		return tipoContatoRepository.save(tipoContato);
	}
	
	public TipoContato buscarPorID(Integer id) {
		if( tipoContatoRepository.findById(id).isPresent() )
			return tipoContatoRepository.findById(id).get();
		return null;
	}
	
	public List<TipoContato> buscarTodos() {
		return (List<TipoContato>) tipoContatoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public TipoContato editar(Integer id, TipoContato tipoContato) {
		tipoContato.setId(id);
		return tipoContatoRepository.save(tipoContato);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(TipoContato tipoContato) {
		tipoContatoRepository.delete(tipoContato);
	}
	
}