package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.ClienteRepository;
import br.com.dbccompany.Repository.ContatoRepository;
import br.com.dbccompany.Repository.TipoContatoRepository;


public class ClienteTest extends CoworkingApplicationTests {
	
	
	@MockBean
	private ClienteRepository clienteRepository;
	
	@MockBean
	private ContatoRepository contatoRepository;
	
	@MockBean
	private TipoContatoRepository tipoContatoRepository;
			
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Cliente cliente = new Cliente();		
		cliente.setNome("Cliente1");
		cliente.setCpf("54654768");
		
		Date date = new Date(11111998);
		cliente.setDataNascimento(date);
		
		List<Contato> contatos = new ArrayList();
		cliente.setContatos(contatos);
		
		TipoContato tipo = new TipoContato();
		tipo.setNome("telefone");
		TipoContato tipo2 = new TipoContato();
		tipo2.setNome("email");
		
		Contato contato = new Contato();
		contato.setValor("51681354");
		Contato contato2 = new Contato();
		contato2.setValor("haihsa@jaiodjasio.com");
		
		contato.setTipoContato(tipo);
		contato2.setTipoContato(tipo2);
		
		contatos.add(contato);
		contatos.add(contato2);
		
		
		Mockito.when(clienteRepository.findByNome(cliente.getNome())).thenReturn(cliente);
		Mockito.when(clienteRepository.findByCpf(cliente.getCpf())).thenReturn(cliente);
		
		
	}
	
	@Test
	public void VerificaNomeCliente() {
		String nome = "Cliente1";
		
		Cliente found = clienteRepository.findByNome(nome);
		
		assertThat(found.getNome()).isEqualTo(nome);
	}
	
		@Test
	public void VerificaDataCliente() {
		String nome = "Cliente1";
		Date date = new Date(11111998);
		
		Cliente found = clienteRepository.findByNome(nome);
		
		assertThat(found.getDataNascimento()).isEqualTo(date);
		
	}
	
	@Test
	public void VerificarCPFCliente() {
		String cpf = "54654768";
		
		Cliente found = clienteRepository.findByCpf(cpf);
		
		assertThat(found.getCpf()).isEqualTo(cpf);
	}
}
