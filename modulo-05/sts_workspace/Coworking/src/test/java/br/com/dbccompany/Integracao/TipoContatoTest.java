package br.com.dbccompany.Integracao;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class TipoContatoTest {

	@MockBean
	private TipoContatoRepository tipoContatoRepository;

	@Autowired
	ApplicationContext context;

	@Before
	public void setUp() {
		TipoContato email = new TipoContato();
		email.setNome("email");
		
		Mockito.when(tipoContatoRepository.findByNome(email.getNome())).thenReturn(email);
	}

	@Test
	public void buscaPorNome() {
		TipoContato found = tipoContatoRepository.findByNome("email");
		assertEquals("email", found.getNome());
	}
}