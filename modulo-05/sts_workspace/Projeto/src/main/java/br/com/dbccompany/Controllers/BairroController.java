package br.com.dbccompany.Controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Service.BairroService;
import br.com.dbccompany.Entity.Bairro;

@Controller
@RequestMapping("/api/bairro")
public class BairroController {
	
	@Autowired
	BairroService bairroService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Bairro> listarTodos() {
		return bairroService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Bairro buscarPorID(@PathVariable Integer id) {
		return bairroService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Bairro salvarNovo(@RequestBody Bairro bairro) {
		return bairroService.salvar(bairro);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Bairro editar(@PathVariable Integer id, @RequestBody Bairro bairro) {
		return bairroService.editar(id, bairro);
	}
}