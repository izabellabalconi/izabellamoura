package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Service.CidadeService;
import br.com.dbccompany.Entity.Cidade;

@Controller
@RequestMapping("/api/cidade")
public class CidadeController {
	
	@Autowired
	CidadeService cidadeService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Cidade> listarTodos() {
		return cidadeService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Cidade buscarPorID(@PathVariable Integer id) {
		return cidadeService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Cidade salvarNovo(@RequestBody Cidade cidade) {
		return cidadeService.salvar(cidade);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Cidade editar(@PathVariable Integer id, @RequestBody Cidade cidade) {
		return cidadeService.editar(id, cidade);
	}

}