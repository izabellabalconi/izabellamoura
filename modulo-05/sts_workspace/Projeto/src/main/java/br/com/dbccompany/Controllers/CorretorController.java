package br.com.dbccompany.Controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Service.CorretorService;
import br.com.dbccompany.Entity.Corretor;

@Controller
@RequestMapping("/api/pessoa/corretor")
public class CorretorController {
	
	@Autowired
	CorretorService corretorService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Corretor> listarTodos() {
		return corretorService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Corretor buscarPorID(@PathVariable Integer id) {
		return corretorService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Corretor salvarNovo(@RequestBody Corretor corretor) {
		return corretorService.salvar(corretor);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Corretor editar(@PathVariable Integer id, @RequestBody Corretor corretor) {
		return corretorService.editar(id, corretor);
	}
}