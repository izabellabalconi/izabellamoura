package br.com.dbccompany.Controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Service.EnderecoSeguradoraService;
import br.com.dbccompany.Entity.EnderecoSeguradora;

@Controller
@RequestMapping("/api/endereco/seguradora")
public class EnderecoSeguradoraController {
	
	@Autowired
	EnderecoSeguradoraService enderecoSeguradoraService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<EnderecoSeguradora> listarTodos() {
		return enderecoSeguradoraService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public EnderecoSeguradora buscarPorID(@PathVariable Integer id) {
		return enderecoSeguradoraService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public EnderecoSeguradora salvarNovo(@RequestBody EnderecoSeguradora enderecoSeguradora) {
		return enderecoSeguradoraService.salvar(enderecoSeguradora);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public EnderecoSeguradora editar(@PathVariable Integer id, @RequestBody EnderecoSeguradora enderecoSeguradora) {
		return enderecoSeguradoraService.editar(id, enderecoSeguradora);
	}
}

