package br.com.dbccompany.Controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Service.EstadoService;
import br.com.dbccompany.Entity.Estado;

@Controller
@RequestMapping("/api/estado")
public class EstadoController {
	
	@Autowired
	EstadoService estadoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Estado> listarTodos() {
		return estadoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Estado buscarPorID(@PathVariable Integer id) {
		return estadoService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Estado salvarNovo(@RequestBody Estado estado) {
		return estadoService.salvar(estado);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Estado editar(@PathVariable Integer id, @RequestBody Estado estado) {
		return estadoService.editar(id, estado);
	}
}
