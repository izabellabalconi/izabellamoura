package br.com.dbccompany.Controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Service.SeguradoraService;
import br.com.dbccompany.Entity.Seguradora;

@Controller
@RequestMapping("/api/seguradora")
public class SeguradoraController {
	
	@Autowired
	SeguradoraService seguradoraService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Seguradora> listarTodos() {
		return seguradoraService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Seguradora buscarPorID(@PathVariable Integer id) {
		return seguradoraService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Seguradora salvarNovo(@RequestBody Seguradora seguradora) {
		return seguradoraService.salvar(seguradora);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Seguradora editar(@PathVariable Integer id, @RequestBody Seguradora seguradora) {
		return seguradoraService.editar(id, seguradora);
	}
}