package br.com.dbccompany.Controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Service.ServicoContratadoService;
import br.com.dbccompany.Entity.ServicoContratado;

@Controller
@RequestMapping("/api/servico/contratado")
public class ServicoContratadoController {
	
	@Autowired
	ServicoContratadoService servicoContratadoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<ServicoContratado> listarTodos() {
		return servicoContratadoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public ServicoContratado buscarPorID(@PathVariable Integer id) {
		return servicoContratadoService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public ServicoContratado salvarNovo(@RequestBody ServicoContratado servicoContratado) {
		return servicoContratadoService.salvar(servicoContratado);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public ServicoContratado editar(@PathVariable Integer id, @RequestBody ServicoContratado servicoContratado) {
		return servicoContratadoService.editar(id, servicoContratado);
	}
}
