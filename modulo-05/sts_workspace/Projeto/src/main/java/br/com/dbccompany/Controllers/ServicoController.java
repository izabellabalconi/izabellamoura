package br.com.dbccompany.Controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Service.ServicoService;
import br.com.dbccompany.Entity.Servico;

@Controller
@RequestMapping("/api/servico")
public class ServicoController {
	
	@Autowired
	ServicoService servicoService;

	@GetMapping(value = "/")
	@ResponseBody
	public List<Servico> listarTodos() {
		return servicoService.buscarTodos();
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Servico buscarPorID(@PathVariable Integer id) {
		return servicoService.buscarPorID(id);		
	}
	
	@PostMapping(value = "/salvar")
	@ResponseBody
	public Servico salvarNovo(@RequestBody Servico servico) {
		return servicoService.salvar(servico);
	}	
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Servico editar(@PathVariable Integer id, @RequestBody Servico servico) {
		return servicoService.editar(id, servico);
	}
}