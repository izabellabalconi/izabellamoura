package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "CORRETOR")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
public class Corretor extends Pessoa{
	
	@Column(nullable = false)
	private String cargo;
	
	@Column(nullable = false)
	private Integer comissao;
	
	@OneToOne(mappedBy = "corretor")
	private ServicoContratado servicoContratado;

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Integer getComissao() {
		return comissao;
	}

	public void setComissao(Integer comissao) {
		this.comissao = comissao;
	}

	public ServicoContratado getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(ServicoContratado servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
	
		
}