package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ENDERECO_SEGURADORA")
@PrimaryKeyJoinColumn(name = "ID_ENDERECO")
public class EnderecoSeguradora extends Endereco{
	
	@OneToOne
	@JoinColumn(name = "ID_SEGURADORA")
	private Seguradora seguradora;

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}
		
}