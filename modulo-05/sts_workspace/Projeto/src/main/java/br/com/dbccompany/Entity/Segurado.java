package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "SEGURADO")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
public class Segurado extends Pessoa{
	
	@Column(nullable = false, name = "QTD_SERVICOS")
	private Integer qtdServicos;
	
	@OneToOne(mappedBy = "segurado")
	private ServicoContratado servicoContratado;

	public Integer getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(Integer qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

	public ServicoContratado getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(ServicoContratado servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
			
}