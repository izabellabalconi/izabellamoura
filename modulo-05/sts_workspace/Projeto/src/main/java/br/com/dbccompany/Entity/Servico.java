package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SERVICO")
public class Servico {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "SERVICO_SEQ", sequenceName = "SERVICO_SEQ")
	@GeneratedValue(generator = "SERVICO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = false)
	private String descricao;
	
	@Column(nullable = false, name = "VALOR_PADRAO")
	private Integer valorPadrao;
	
	@ManyToMany(mappedBy = "servicos")
	private List<Seguradora> seguradoras = new ArrayList<>();
	
	@OneToOne(mappedBy = "servico")
	private ServicoContratado servicoContratado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(Integer valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	public List<Seguradora> getSeguradoras() {
		return seguradoras;
	}

	public void pushSeguradoras(Seguradora... seguradoras) {
		this.seguradoras.addAll(Arrays.asList(seguradoras));
	}

	public ServicoContratado getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(ServicoContratado servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
		

}