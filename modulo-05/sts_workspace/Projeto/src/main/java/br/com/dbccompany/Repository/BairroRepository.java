package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;

public interface BairroRepository extends CrudRepository<Bairro, Integer>{
	Bairro findByNome(String nome);
	List<Bairro> findAllByCidade(Cidade cidade);
}