package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Corretor;

public interface CorretorRepository extends CrudRepository<Corretor, Integer>{
	List<Corretor> findAllByCargo(String cargo);
	List<Corretor> findAllByComissao(Integer comissao);
	Corretor findByNome(String nome);
	Corretor findByCpf(String cpf);
	Corretor findByPai(String pai);
	Corretor findByMae(String mae);
	Corretor findByTelefone(String telefone);
	Corretor findByEmail(String email);	
}
