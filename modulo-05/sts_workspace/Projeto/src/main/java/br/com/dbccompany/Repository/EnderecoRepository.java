package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Pessoa;

public interface EnderecoRepository extends CrudRepository<Endereco, Integer>{
	List<Endereco> findAllByLogradouro(String logradouro);
	List<Endereco> findAllByNumero(Integer numero);
	List<Endereco> findAllByBairro(Bairro bairro);
	List<Endereco> findAllByPessoas(Pessoa pessoa);
}