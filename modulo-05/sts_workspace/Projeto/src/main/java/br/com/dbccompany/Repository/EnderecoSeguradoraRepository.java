package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.EnderecoSeguradora;
import br.com.dbccompany.Entity.Seguradora;

public interface EnderecoSeguradoraRepository extends CrudRepository<EnderecoSeguradora, Integer>{
EnderecoSeguradora findBySeguradora(Seguradora seguradora);
}