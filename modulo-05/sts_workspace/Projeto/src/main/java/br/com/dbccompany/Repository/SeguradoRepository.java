package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Segurado;

public interface SeguradoRepository extends CrudRepository<Segurado, Integer>{
	List<Segurado> findAllByQtdServicos(Integer qtdServicos);
	Segurado findByNome(String nome);
	Segurado findByCpf(String cpf);
	Segurado findByPai(String pai);
	Segurado findByMae(String mae);
	Segurado findByTelefone(String telefone);
	Segurado findByEmail(String email);
}