package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.EnderecoSeguradora;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.ServicoContratado;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Integer>{
	Seguradora findByNome(String nome);
	Seguradora findByCnpj(String cnpj);
	Seguradora findByEnderecoSeguradora(EnderecoSeguradora endereco);
	Seguradora findByServicoContratado(ServicoContratado servico);
}