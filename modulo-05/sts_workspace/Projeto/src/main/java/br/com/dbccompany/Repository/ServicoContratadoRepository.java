package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.Segurado;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Entity.ServicoContratado;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Integer>{
	List<ServicoContratado> findAllByDescricao(String descricao);
	List<ServicoContratado> findAllByValor(Integer valor);
	List<ServicoContratado> findAllByServico(Servico servico);
	List<ServicoContratado> findAllBySeguradora(Seguradora seguradora);
	List<ServicoContratado> findAllBySegurado(Segurado segurado);
	List<ServicoContratado> findAllByCorretor(Corretor corretor);
}
