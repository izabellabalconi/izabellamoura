package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Entity.ServicoContratado;

public interface ServicoRepository extends CrudRepository<Servico, Integer>{
	List<Servico> findAllByNome(String nome);
	List<Servico> findAllByDescricao(String descricao);
	List<Servico> findAllByValorPadrao(Integer valor);
	Servico findByServicoContratado(ServicoContratado servico);
}