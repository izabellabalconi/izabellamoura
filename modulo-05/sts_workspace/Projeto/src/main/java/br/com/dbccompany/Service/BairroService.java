package br.com.dbccompany.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Repository.BairroRepository;

@Service
public class BairroService {

	@Autowired
	public BairroRepository bairroRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro salvar(Bairro bairro) {
		return bairroRepository.save(bairro);
	}
	
	public Bairro buscarPorID(Integer id) {
		if( bairroRepository.findById(id).isPresent() )
			return bairroRepository.findById(id).get();
		return null;
	}
	
	public List<Bairro> buscarTodos() {
		return (List<Bairro>) bairroRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro editar(Integer id, Bairro bairro) {
		bairro.setId(id);
		return bairroRepository.save(bairro);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Bairro bairro) {
		bairroRepository.delete(bairro);
	}
	
}
