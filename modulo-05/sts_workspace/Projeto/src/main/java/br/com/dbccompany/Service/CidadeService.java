package br.com.dbccompany.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Repository.CidadeRepository;

@Service
public class CidadeService {
	
	@Autowired
	public CidadeRepository cidadeRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade salvar(Cidade cidade) {
		return cidadeRepository.save(cidade);
	}
	
	public Cidade buscarPorID(Integer id) {
		if( cidadeRepository.findById(id).isPresent() )
			return cidadeRepository.findById(id).get();
		return null;
	}
	
	public List<Cidade> buscarTodos() {
		return (List<Cidade>) cidadeRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade editar(Integer id, Cidade cidade) {
		cidade.setId(id);
		return cidadeRepository.save(cidade);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Cidade cidade) {
		cidadeRepository.delete(cidade);
	}
	
}
