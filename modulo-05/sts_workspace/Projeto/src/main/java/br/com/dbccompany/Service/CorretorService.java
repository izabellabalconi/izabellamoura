package br.com.dbccompany.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Repository.CorretorRepository;


@Service
public class CorretorService {

	@Autowired
	public CorretorRepository corretorRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor salvar(Corretor corretor) {
		return corretorRepository.save(corretor);
	}
	
	public Corretor buscarPorID(Integer id) {
		if( corretorRepository.findById(id).isPresent() )
			return corretorRepository.findById(id).get();
		return null;
	}
	
	public List<Corretor> buscarTodos() {
		return (List<Corretor>) corretorRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor editar(Integer id, Corretor corretor) {
		corretor.setId(id);
		return corretorRepository.save(corretor);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Corretor corretor) {
		corretorRepository.delete(corretor);
	}
	
}
