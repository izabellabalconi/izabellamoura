package br.com.dbccompany.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.EnderecoSeguradora;
import br.com.dbccompany.Repository.EnderecoSeguradoraRepository;

@Service
public class EnderecoSeguradoraService {

	@Autowired
	public EnderecoSeguradoraRepository enderecoSeguradoraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradora salvar(EnderecoSeguradora enderecoSeguradora) {
		return enderecoSeguradoraRepository.save(enderecoSeguradora);
	}
	
	public EnderecoSeguradora buscarPorID(Integer id) {
		if( enderecoSeguradoraRepository.findById(id).isPresent() )
			return enderecoSeguradoraRepository.findById(id).get();
		return null;
	}
	
	public List<EnderecoSeguradora> buscarTodos() {
		return (List<EnderecoSeguradora>) enderecoSeguradoraRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradora editar(Integer id, EnderecoSeguradora enderecoSeguradora) {
		enderecoSeguradora.setId(id);
		return enderecoSeguradoraRepository.save(enderecoSeguradora);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(EnderecoSeguradora enderecoSeguradora) {
		enderecoSeguradoraRepository.delete(enderecoSeguradora);
	}
	
}