package br.com.dbccompany.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.EnderecoRepository;

@Service
public class EnderecoService {

	@Autowired
	public EnderecoRepository enderecoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco salvar(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}
	
	public Endereco buscarPorID(Integer id) {
		if( enderecoRepository.findById(id).isPresent() )
			return enderecoRepository.findById(id).get();
		return null;
	}
	
	public List<Endereco> buscarTodos() {
		return (List<Endereco>) enderecoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco editar(Integer id, Endereco endereco) {
		endereco.setId(id);
		return enderecoRepository.save(endereco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Endereco endereco) {
		enderecoRepository.delete(endereco);
	}
	
}
