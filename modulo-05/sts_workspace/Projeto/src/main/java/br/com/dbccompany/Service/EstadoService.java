package br.com.dbccompany.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;

@Service
public class EstadoService {
	
	@Autowired
	public EstadoRepository estadoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Estado salvar(Estado estado) {
		return estadoRepository.save(estado);
	}
	
	public Estado buscarPorID(Integer id) {
		if( estadoRepository.findById(id).isPresent() )
			return estadoRepository.findById(id).get();
		return null;
	}
	
	public List<Estado> buscarTodos() {
		return (List<Estado>) estadoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Estado editar(Integer id, Estado estado) {
		estado.setId(id);
		return estadoRepository.save(estado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Estado estado) {
		estadoRepository.delete(estado);
	}

}