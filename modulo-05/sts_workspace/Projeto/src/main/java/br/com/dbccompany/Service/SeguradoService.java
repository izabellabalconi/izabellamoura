package br.com.dbccompany.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Segurado;
import br.com.dbccompany.Repository.SeguradoRepository;

@Service
public class SeguradoService {

	@Autowired
	public SeguradoRepository seguradoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Segurado salvar(Segurado segurado) {
		return seguradoRepository.save(segurado);
	}
	
	public Segurado buscarPorID(Integer id) {
		if( seguradoRepository.findById(id).isPresent() )
			return seguradoRepository.findById(id).get();
		return null;
	}
	
	public List<Segurado> buscarTodos() {
		return (List<Segurado>) seguradoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Segurado editar(Integer id, Segurado segurado) {
		segurado.setId(id);
		return seguradoRepository.save(segurado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Segurado segurado) {
		seguradoRepository.delete(segurado);
	}
	
}