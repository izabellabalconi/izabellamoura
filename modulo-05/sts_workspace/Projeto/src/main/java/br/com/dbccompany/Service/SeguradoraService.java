package br.com.dbccompany.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.SeguradoraRepository;

@Service
public class SeguradoraService {

	@Autowired
	public SeguradoraRepository seguradoraRepository;

	@Transactional(rollbackFor = Exception.class)
	public Seguradora salvar(Seguradora seguradora) {
		return seguradoraRepository.save(seguradora);
	}
	
	public Seguradora buscarPorID(Integer id) {
		if( seguradoraRepository.findById(id).isPresent() )
			return seguradoraRepository.findById(id).get();
		return null;
	}
	
	public List<Seguradora> buscarTodos() {
		return (List<Seguradora>) seguradoraRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora editar(Integer id, Seguradora seguradora) {
		seguradora.setId(id);
		return seguradoraRepository.save(seguradora);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Seguradora seguradora) {
		seguradoraRepository.delete(seguradora);
	}
	
}