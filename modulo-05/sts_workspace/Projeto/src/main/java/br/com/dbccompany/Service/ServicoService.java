package br.com.dbccompany.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Repository.ServicoRepository;

@Service
public class ServicoService {

	@Autowired
	public ServicoRepository servicoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Servico salvar(Servico servico) {
		return servicoRepository.save(servico);
	}
	
	public Servico buscarPorID(Integer id) {
		if( servicoRepository.findById(id).isPresent() )
			return servicoRepository.findById(id).get();
		return null;
	}
	
	public List<Servico> buscarTodos() {
		return (List<Servico>) servicoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servico editar(Integer id, Servico servico) {
		servico.setId(id);
		return servicoRepository.save(servico);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Servico servico) {
		servicoRepository.delete(servico);
	}
	
}
