package br.com.dbccompany.Integracao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.dbccompany.ProjetoApplicationTests;
import br.com.dbccompany.Controllers.EstadoController;

public class EstadoIntegration3Tests extends ProjetoApplicationTests {
	
	private MockMvc mvc;
	
	@Autowired
	private EstadoController controller;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.standaloneSetup(controller).build();
	}
	
	@Test
	public void statusOk() throws Exception {
		this.mvc.perform(MockMvcRequestBuilders.get("/api/estado/")).andExpect(MockMvcResultMatchers.status().isOk());
	}
}
