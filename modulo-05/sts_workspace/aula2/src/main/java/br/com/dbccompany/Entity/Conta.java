package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
@Table(name = "CONTA")
public class Conta {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "conta_seq", sequenceName = "conta_seq")
    @GeneratedValue(generator = "conta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "ID_AGENCIA")
    private Agencia agencia;
    
    private Integer numero;
    
    private Integer saldo;
    
    /*  @Enumerated(EnumType.STRING)
    @JoinColumn(name = "TIPO_CONTA", nullable = false)
    private TipoConta tipoConta; */
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CONTA_CLIENTE", joinColumns = {
        @JoinColumn(name = "ID_CONTA")}, inverseJoinColumns = {
            @JoinColumn(name = "ID_CLIENTE")})
    private List<Cliente> clientes = new ArrayList<>();
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getSaldo() {
        return saldo;
    }

    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void pushClientes(Cliente... clientes) {
        this.clientes.addAll(Arrays.asList(clientes));
    }

      

 }
    