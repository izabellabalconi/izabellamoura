package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "MOVIMENTACAO")
public class Movimentacao {
    
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
    
    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "TIPO_MOVIMENTACAO", nullable = false)
    private TipoMovimentacao tipoMovimentacao;
    
    @ManyToOne
    @JoinColumn(name = "ID_CONTA_ORIGEM")
    private Conta contaOrigem;
    
    @ManyToOne
    @JoinColumn(name = "ID_CONTA_DESTINO")
    private Conta contaDestino;
    
    private String data;
    
    private Integer valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoMovimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public Conta getContaOrigem() {
        return contaOrigem;
    }

    public void setContaOrigem(Conta contaOrigem) {
        this.contaOrigem = contaOrigem;
    }

    public Conta getContaDestino() {
        return contaDestino;
    }

    public void setContaDestino(Conta contaDestino) {
        this.contaDestino = contaDestino;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
    
    
}
