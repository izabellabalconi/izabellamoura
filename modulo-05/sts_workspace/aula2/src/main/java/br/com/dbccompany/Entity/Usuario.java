package br.com.dbccompany.Entity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "USUARIO")
@Inheritance(strategy = InheritanceType.JOINED)
public class Usuario{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "usuario_seq", sequenceName = "usuario_seq")
    @GeneratedValue(generator = "usuario_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;

    private Long cpf;
    
    private Long rg;
    
    @ManyToOne
    @JoinColumn(name = "ID_BANCO")
    private Banco banco;
    
    @ManyToMany
    @JoinTable(name = "usuario_endereco",
            joinColumns = {
                @JoinColumn(name = "id_usuario")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_endereco")})
    private List<Endereco> enderecos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<Contato> contatos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public Long getRg() {
		return rg;
	}

	public void setRg(Long rg) {
		this.rg = rg;
	}	

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void pushEnderecos(Endereco... enderecos) {
        this.enderecos.addAll(Arrays.asList(enderecos));
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos){
        this.contatos.addAll(Arrays.asList(contatos));
    }

}