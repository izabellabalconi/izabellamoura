package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;

public interface AgenciaRepository extends CrudRepository<Agencia, Long>{ 
	Agencia findByCodigo(Long codigo);
}
