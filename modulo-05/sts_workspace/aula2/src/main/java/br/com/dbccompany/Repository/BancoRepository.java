package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Banco;

public interface BancoRepository extends CrudRepository<Banco, Long>{
	Banco findByCodigo(Long codigo);

}
