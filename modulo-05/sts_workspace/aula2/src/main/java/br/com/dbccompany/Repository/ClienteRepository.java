package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{ 
	Cliente findByCpf(Long cpf);
	Cliente findByRg(Long rg);
	Cliente findByNome(String nome);
}
