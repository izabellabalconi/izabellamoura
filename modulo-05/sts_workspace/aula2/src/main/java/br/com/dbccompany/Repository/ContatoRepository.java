package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Contato;

public interface ContatoRepository extends CrudRepository<Contato, Long>{

}
