package br.com.dbccompany.Repository;


import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.EmprestimoAprovado;

public interface EmprestimoAprovadoRepository extends CrudRepository<EmprestimoAprovado, Integer>{
	EmprestimoAprovado findByEmprestimo(Emprestimo emprestimo);
}
