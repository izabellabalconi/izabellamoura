package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Endereco;



public interface EnderecoRepository extends CrudRepository<Endereco, Long> {
	Endereco findByLogradouro(String Logradouro);
	Endereco findByNumero(Long numero);
	Endereco findByBairro(String bairro);
	Endereco findByCidade(String cidade);

}
