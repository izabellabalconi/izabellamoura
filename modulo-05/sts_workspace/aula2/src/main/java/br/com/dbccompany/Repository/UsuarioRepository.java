package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	Usuario findByCpf(Long cpf);
	Usuario findByRg(Long rg);
	Usuario findByNome(String nome);
}
