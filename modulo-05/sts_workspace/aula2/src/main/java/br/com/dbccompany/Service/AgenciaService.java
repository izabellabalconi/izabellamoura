package br.com.dbccompany.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Repository.AgenciaRepository;
import br.com.dbccompany.Repository.BancoRepository;

public class AgenciaService {
	@Autowired
	
	private AgenciaRepository agenciaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Agencia salvar(Agencia agencia) {
		return agenciaRepository.save(agencia);
	}
	private Optional<Agencia> buscarAgencia(long id) {
		return agenciaRepository.findById(id);		
	}
}
