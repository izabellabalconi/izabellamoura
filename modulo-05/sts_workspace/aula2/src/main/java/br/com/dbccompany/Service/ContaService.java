package br.com.dbccompany.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Repository.BancoRepository;
import br.com.dbccompany.Repository.ContaRepository;

public class ContaService {
	@Autowired
	
	private ContaRepository contaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Conta salvar(Conta conta) {
		return contaRepository.save(conta);
	}
	private Optional<Conta> buscarConta(long id) {
		return contaRepository.findById(id);		
	}

}
