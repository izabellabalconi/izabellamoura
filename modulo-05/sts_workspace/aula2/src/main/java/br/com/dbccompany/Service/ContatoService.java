package br.com.dbccompany.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.BancoRepository;
import br.com.dbccompany.Repository.ContatoRepository;

public class ContatoService {
	@Autowired
	
	private ContatoRepository contatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Contato salvar(Contato contato) {
		return contatoRepository.save(contato);
	}
	private Optional<Contato> buscarContato(long id) {
		return contatoRepository.findById(id);		
	}

}
