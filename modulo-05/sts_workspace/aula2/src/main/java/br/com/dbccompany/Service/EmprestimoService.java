package br.com.dbccompany.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Repository.EmprestimoRepository;

public class EmprestimoService {

	@Autowired
	private EmprestimoRepository emprestimoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Emprestimo salvar(Emprestimo emprestimo) {
		return emprestimoRepository.save(emprestimo);
	}
	
	private Emprestimo buscarEmprestimo(Integer id) {
		if ( emprestimoRepository.findById(id).isPresent() )
			return emprestimoRepository.findById(id).get();
		return null;
	}
	
	@Transactional(rollbackFor = Exception.class)
	private Emprestimo editarEmprestimo(Integer id, Emprestimo emprestimo) {
		emprestimo.setId(id);
		return emprestimoRepository.save(emprestimo);
	}
}
