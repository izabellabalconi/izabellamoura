 package br.com.dbccompany.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.BancoRepository;
import br.com.dbccompany.Repository.UsuarioRepository;

public class UsuarioService {
	@Autowired
	
	private UsuarioRepository usuarioRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Usuario salvar(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	private Optional<Usuario> buscarUsuario(long id) {
		return usuarioRepository.findById(id);		
	}

}
